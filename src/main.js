import '@babel/polyfill'
import 'mutationobserver-shim'
import './plugins/bootstrap-vue'

import Vue from 'vue'
import App from './App.vue'

//----------------------------------------------------------------------------------------------------------------------
// routes
//----------------------------------------------------------------------------------------------------------------------
import VueRouter from 'vue-router';
import List from "./components/list";

Vue.use(VueRouter);

const routes = [
  {
    path: '',
    name: 'items',
    component: List,
    meta: { title: 'List' }
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

//----------------------------------------------------------------------------------------------------------------------

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
