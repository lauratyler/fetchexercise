# Fetch Front End Test

### Getting Started

Install dependencies using
```
npm install
```

Start application with 
```
npm run serve
```

App will be running at http://localhost:8080

### Displays

#### Table

Table shows the records provided that are pulled from the static site. The button shows the number of
records with that listID. To the right is the item names.

The eventual goal would be to utilize the VueRouter that has been established to show off a variety of graphics for the user.



